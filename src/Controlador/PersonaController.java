/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Dao.PersonaImpl;
import Entidad.Persona;

/**
 *
 * @author cepardov
 */
public class PersonaController extends Persona {
    private PersonaImpl personaImpl = new PersonaImpl();
    
    public Object[][] getPersona(){
        return personaImpl.getPersona();
    }
    
    public boolean save(){
        return personaImpl.save(this);
    }
    
    public boolean update(){
        return personaImpl.update(this);
    }
    
    public boolean delete(){
        return personaImpl.delete(this);
    }
    
    public void findByRut(){
        personaImpl.findByRut(this);
    }
    
    public String[] getComboPersona(){
        return personaImpl.getComboPersona();
    }
    
    public String[] getComboPersonas(){
        return personaImpl.getComboPersonas();
    }
}
