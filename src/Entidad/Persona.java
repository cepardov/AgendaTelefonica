/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidad;

/**
 *
 * @author cepardov
 */
public class Persona {
    /**
     * Primero: Creamos las clases entidad, que basicamente son las
     * tablas de la base de datos.
     * 
     * Segundo: Se deben crear las variables que llevan el mismo nombre
     * de los datos de la tabla usando la encapsulacion.
     * 
     * Tercero: Se deben crar los getters y setters para cambiar el contenido
     * de los datos de cada variable.
     * 
     * Puedes investigar mas sobre encapsulación aqui:
     * https://es.wikibooks.org/wiki/Programaci%C3%B3n_en_Java/Encapsulamiento
     * http://www.ciberaula.com/articulo/encapsulacion_java
     * https://i.stack.imgur.com/niONO.png
     */
    protected int idPersona; //Debe ser una variable protegida, puede ser accedida por subclases del paquete.
    protected String rutPersona;
    protected String nombrePersona;
    protected String paternoPersona;
    protected String maternoPersona;
    protected String nacimientoPersona;
    protected int idUsuario;
    protected int idTelefono;
    protected int idCorreo;

    /**
     * Este es un metodo publico puede ser accedido de subclases fuera del paquete
     * se obtiene el dato si este esta precargado
     * @return Retorna el contenido de la variable idPersona
     */
    public int getIdPersona() {
        return idPersona;
    }

    public String getRutPersona() {
        return rutPersona;
    }

    public void setRutPersona(String rutPersona) {
        this.rutPersona = rutPersona;
    }
    
    /**
     * Este metodo publico permite escribir el contenido de la variable idPersona
     * @param idPersona 
     */
    public void setIdPersona(int idPersona) {
        this.idPersona = idPersona;
    }

    public String getNombrePersona() {
        return nombrePersona;
    }

    public void setNombrePersona(String nombrePersona) {
        this.nombrePersona = nombrePersona;
    }

    public String getPaternoPersona() {
        return paternoPersona;
    }

    public void setPaternoPersona(String paternoPersona) {
        this.paternoPersona = paternoPersona;
    }

    public String getMaternoPersona() {
        return maternoPersona;
    }

    public void setMaternoPersona(String maternoPersona) {
        this.maternoPersona = maternoPersona;
    }

    public String getNacimientoPersona() {
        return nacimientoPersona;
    }

    public void setNacimientoPersona(String nacimientoPersona) {
        this.nacimientoPersona = nacimientoPersona;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public int getIdCorreo() {
        return idCorreo;
    }

    public void setIdCorreo(int idCorreo) {
        this.idCorreo = idCorreo;
    }

    public int getIdTelefono() {
        return idTelefono;
    }

    public void setIdTelefono(int idTelefono) {
        this.idTelefono = idTelefono;
    }
}
