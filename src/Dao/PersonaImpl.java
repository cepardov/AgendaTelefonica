/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Util.DataBaseInstance;
import static Util.DataBaseInstance.closeConnection;
import static Util.DataBaseInstance.stateConnection;
import Entidad.Persona;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;

/**
 *
 * @author cepardov
 */
public class PersonaImpl {
    protected Connection getConnection(){
        return DataBaseInstance.getInstanceConnection();
    }
    
    //CRUD
    public Object [][] getPersona(){
        int posId = 0;
        
        try {
            //ps position
            PreparedStatement pstm = getConnection().prepareStatement("SELECT count(1) as total FROM persona");
            ResultSet res = pstm.executeQuery();
            
            res.next();
            posId = res.getInt("total");
            
        } catch (SQLException e){
            System.out.println("Error: "+e);
        }
        
        Object[][] data = new String[posId][9];
        
        try{
            PreparedStatement pstm = getConnection().prepareStatement("SELECT idPersona, rutPersona, nombrePersona, paternoPersona, maternoPersona, nacimientoPersona FROM persona ORDER BY idPersona");
            ResultSet res = pstm.executeQuery();
            
            int increment = 0;
            
            while(res.next()){
                String idPersona = res.getString("idPersona");
                String rutPersona = res.getString("rutPersona");
                String nombrePersona = res.getString("nombrePersona");
                String paternoPersona = res.getString("paternoPersona");
                String maternoPersona = res.getString("maternoPersona");
                String nacimientoPersona = res.getString("nacimientoPersona");
                
                data[increment][0] = idPersona;
                data[increment][1] = rutPersona;
                data[increment][2] = nombrePersona;
                data[increment][3] = paternoPersona;
                data[increment][4] = maternoPersona;
                data[increment][5] = nacimientoPersona;
                
                increment++;
            }
            res.close();
            closeConnection();
        } catch (SQLException e){
            System.out.println("Error: "+e);
        }
        
        return data;
    }
    
    public String[] getComboPersona(){
        int posId = 0;
        try {
            //ps position
            PreparedStatement pstm = getConnection().prepareStatement("SELECT count(1) as total FROM persona");
            ResultSet res = pstm.executeQuery();
            
            res.next();
            posId = res.getInt("total");
            
        } catch (SQLException e){
            System.out.println("Error: "+e);
        }
        
        String[] dat = new String[posId];
        try{
            PreparedStatement pstm = getConnection().prepareStatement("SELECT idPersona,rutPersona, nombrePersona, paternoPersona, maternoPersona, nacimientoPersona FROM persona ORDER BY idPersona");
            ResultSet res = pstm.executeQuery();
            
            int increment = 0;
            
            while(res.next()){
                String rutPersona = res.getString("rutPersona");
                
                dat[increment] = rutPersona;
                
                increment++;
            }
            res.close();
            closeConnection();
            
            return dat;
        } catch (SQLException e){
            System.out.println("Error: "+e);
            return null;
        }
    }
    
    public String[] getComboPersonas(){
        int posId = 0;
        try {
            //ps position
            PreparedStatement pstm = getConnection().prepareStatement("SELECT count(1) as total FROM persona");
            ResultSet res = pstm.executeQuery();
            
            res.next();
            posId = res.getInt("total");
            
        } catch (SQLException e){
            System.out.println("Error: "+e);
        }
        
        String[] dat = new String[posId];
        try{
            PreparedStatement pstm = getConnection().prepareStatement("SELECT idPersona,rutPersona, nombrePersona, paternoPersona, maternoPersona, nacimientoPersona FROM persona ORDER BY idPersona");
            ResultSet res = pstm.executeQuery();
            
            int increment = 0;
            
            while(res.next()){
                String rutPersona = res.getString("rutPersona");
                
                dat[increment] = rutPersona;
                
                increment++;
            }
            res.close();
            closeConnection();
            
            return dat;
        } catch (SQLException e){
            System.out.println("Error: "+e);
            return null;
        }
    }
    
    public void findByRut(Persona persona) {
        String rutPersona = persona.getRutPersona();
        System.out.println("Rut="+rutPersona);
        try{
            PreparedStatement pstm = getConnection().prepareStatement("SELECT * FROM persona WHERE rutPersona = \""+rutPersona+"\"");
            ResultSet res = pstm.executeQuery();
            
            if (!res.next()) {
                throw new SQLException();
            }
            System.out.println("res="+res.getInt("idPersona"));
            persona.setIdPersona(res.getInt("idPersona"));
            
            res.close();
            pstm.close();
            closeConnection();
            
        } catch (SQLException ex) {
            Logger.getLogger(PersonaImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public boolean save(Persona persona){
        PreparedStatement savePersona;
        
        try{
            savePersona = this.getConnection().prepareStatement(
                    "INSERT INTO persona (rutPersona, nombrePersona, paternoPersona, maternoPersona, nacimientoPersona) "
                            + "VALUES (?,?,?,?,?)");
            savePersona.setString(1, persona.getRutPersona());
            savePersona.setString(2, persona.getNombrePersona());
            savePersona.setString(3, persona.getPaternoPersona());
            savePersona.setString(4, persona.getMaternoPersona());
            savePersona.setString(5, persona.getNacimientoPersona());
            
            savePersona.executeUpdate(); //desde JDBC driver 5 solo las consultas se hacen con executeUpdate
            
            return true;
        } catch (SQLException e){
            System.out.println("Error: "+e);
            return false;
        } finally {
            closeConnection();
        }
    }
    
    /**
     * Este metodo actualiza una persona de la base de datos
     * @param persona
     */
    public boolean update(Persona persona){
        PreparedStatement updatePersona;
        
        try{
            updatePersona = getConnection().prepareStatement("UPDATE persona SET rutPersona=?, nombrePersona=?, paternoPersona=?, maternoPersona=?, nacimientoPersona=?"
                    + "WHERE idPersona=?");
            updatePersona.setString(1, persona.getRutPersona());
            updatePersona.setString(2, persona.getNombrePersona());
            updatePersona.setString(3, persona.getPaternoPersona());
            updatePersona.setString(4, persona.getMaternoPersona());
            updatePersona.setString(5, persona.getNacimientoPersona());
            updatePersona.setInt(6, persona.getIdPersona());
            
            updatePersona.executeUpdate();
            return true;
        } catch (SQLException e){
            System.out.println("Error: "+e);
            return false;
        } finally {
            closeConnection();
        }
    }
    
    public boolean delete(Persona persona){
        PreparedStatement delPersona;
        
        try{
            if(Integer.toString(persona.getIdPersona()) != null){
                delPersona = getConnection().prepareStatement("DELETE FROM persona WHERE idPersona = ?");
                
                delPersona.setInt(1, persona.getIdPersona());
                
                delPersona.executeUpdate();
                return true;
            }
        } catch (SQLException e){
            System.out.println("Error = "+e);
            return false;
        } finally {
            closeConnection();
        }
        return false;
    }
}
