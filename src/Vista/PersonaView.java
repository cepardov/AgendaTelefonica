/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Controlador.PersonaController;
import javax.swing.DefaultComboBoxModel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author cepardov
 */
public class PersonaView extends javax.swing.JInternalFrame {

    PersonaController pc = new PersonaController();
    
    /**
     * Creates new form PersonaView
     */
    public PersonaView() {
        initComponents();
        this.lblEstado.setVisible(false);
        this.btnActualizar.setEnabled(false);
        
        this.actualizarTabla();
        
        this.sinTexto();
        
        this.setComboAño();
        
        this.setComboPersona();
        
        this.setComboPersonas();
    }
    
    private void limpiar(){
        this.txtId.setText("");
        this.txtRut.setText("");
        this.txtNombre.setText("");
        this.txtPaterno.setText("");
        this.txtMaterno.setText("");
        this.cbAnio.setSelectedIndex(0);
        
        this.sinTexto();
        
        this.actualizarTabla();
    }
    
    private void conTexto(){
        
        this.btnGuardar.setEnabled(false);
        this.btnActualizar.setEnabled(true);
        this.btnEliminar.setEnabled(true);
        
        this.actualizarTabla();
        this.setComboPersonas();
    }
    
    private void sinTexto(){
        this.btnGuardar.setEnabled(true);
        this.btnActualizar.setEnabled(false);
        this.btnEliminar.setEnabled(false);
        
        this.cbMes.setEnabled(false);
        this.cbDia.setEnabled(false);
    }
    
    private void setComboAño(){
        DefaultComboBoxModel modeloCombo = new DefaultComboBoxModel();
        modeloCombo.addElement("Año");
        for (int i = 1900; i < 2050; i++) {
            modeloCombo.addElement(i);
        }
        this.cbAnio.setModel(modeloCombo);
    }
    
    private void setComboDia(int mes, int año){
        DefaultComboBoxModel modeloCombo = new DefaultComboBoxModel();
        int numDias = 0;
        switch (mes) {
            case 1: case 3: case 5: case 7: case 8: case 10: case 12:
                numDias = 31;
                break;
            case 4: case 6: case 9: case 11:
                numDias = 30;
                break;
            case 2:
                if((año%4==0 && año%100!=0) || año%400==0){
                    numDias = 29;
                }
                else{
                    numDias = 28;
                }
                break;
        }
        modeloCombo.addElement("Día");
        for (int i = 1; i < numDias+1; i++) {
            modeloCombo.addElement(i);
        }
        this.cbDia.setModel(modeloCombo);
    }
    
    private String getFechaForm(String dia, String mes, String año){
        String dato = null;
        switch (mes){
            case "Enero": dato = "01" ;
                break;
            case "Febrero": dato = "02" ;
                break;
            case "Marzo": dato = "03";
                break;
            case "Abril": dato = "04";
                break;
            case "Mayo": dato = "05";
                break;
            case "Junio": dato = "06";
                break;
            case "Julio": dato = "07";
                break;
            case "Agosto": dato = "08";
                break;
            case "Septiembre": dato = "09";
                break;
            case "Octubre": dato = "10";
                break;
            case "Noviembre": dato = "11";
                break;
            case "Diciembre": dato = "12";
                break;
        }
        
        String nacimientoPersona = año+"/"+dato+"/"+dia;
        
        return nacimientoPersona;
    }
    
    private void actualizarTabla(){
        Object [][] tablaPersona;
        tablaPersona = pc.getPersona();
        
        String[] nombreColumna = {"ID","RUT","Nombre","Paterno","Materno","Nacimiento"};
        
        DefaultTableModel datos = new DefaultTableModel(tablaPersona,nombreColumna);
        tabla.setModel(datos);
    }
    
    private boolean setDatos(){
        int idPersona = 0;
        if(!this.txtId.getText().isEmpty()){
            idPersona = Integer.parseInt(this.txtId.getText());
        }
        String rutPersona = this.txtRut.getText();
        String nombrePersona = this.txtNombre.getText();
        String paternoPersona = this.txtPaterno.getText();
        String maternoPersona = this.txtMaterno.getText();
               
        if(rutPersona.isEmpty()){
            this.estado(false, "Debe ingresar Rut");
            return false;
        } else if (nombrePersona.isEmpty()){
            this.estado(false, "Debe ingresar Nombre");
            return false;
        } else if(this.cbAnio.getSelectedIndex()<1){
            this.estado(false, "Debe Ingresar Año");
        } else if(this.cbMes.getSelectedIndex()<1){
            this.estado(false, "Debe ingresar Mes");
            return false;
        } else if (this.cbDia.getSelectedIndex()<1){ 
            this.estado(false, "Debe Ingresar Día");
            return false;
        } else {
            String año = this.cbAnio.getSelectedItem().toString();
            String dia = this.cbDia.getSelectedItem().toString();
            String mes = this.cbMes.getSelectedItem().toString();
            
            pc.setIdPersona(idPersona);
            pc.setRutPersona(rutPersona);
            pc.setNombrePersona(nombrePersona);
            pc.setPaternoPersona(paternoPersona);
            pc.setMaternoPersona(maternoPersona);
            pc.setNacimientoPersona(this.getFechaForm(dia, mes, año));
            return true;
        }
        return false;
    }
    
    private void estado(boolean result, String text){
        if(result){
            this.lblEstado.setText(text);
            this.lblEstado.setForeground(java.awt.Color.green);
            this.lblEstado.setVisible(true);
        } else {
            this.lblEstado.setText(text);
            this.lblEstado.setForeground(java.awt.Color.red);
            this.lblEstado.setVisible(true);
        }
    }
    
    private void setComboPersonas(){
         this.cbPersona.setModel(new javax.swing.DefaultComboBoxModel<>(pc.getComboPersona()));
     }
    
    private void setComboPersona(){
        this.comboPersona.setModel(new javax.swing.DefaultComboBoxModel<>(pc.getComboPersonas()));
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtId = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        txtRut = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        txtNombre = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        txtPaterno = new javax.swing.JTextField();
        txtMaterno = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        cbDia = new javax.swing.JComboBox<>();
        jLabel6 = new javax.swing.JLabel();
        cbMes = new javax.swing.JComboBox<>();
        cbAnio = new javax.swing.JComboBox<>();
        jButton1 = new javax.swing.JButton();
        btnGuardar = new javax.swing.JButton();
        lblEstado = new javax.swing.JLabel();
        btnActualizar = new javax.swing.JButton();
        btnLimpiar = new javax.swing.JButton();
        btnEliminar = new javax.swing.JButton();
        btnBuscar = new javax.swing.JButton();
        cbPersona = new javax.swing.JComboBox<>();
        comboPersona = new javax.swing.JComboBox<>();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tabla = new javax.swing.JTable();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        setTitle("Mantenedor Persona");

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Datos Persona"));

        jLabel1.setText("ID");

        txtId.setEditable(false);
        txtId.setEnabled(false);

        jLabel2.setText("RUT");

        jLabel3.setText("Nombre");

        jLabel4.setText("Paterno");

        jLabel5.setText("Materno");

        jLabel6.setText("Nacimiento");

        cbMes.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Seleccione", "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre" }));
        cbMes.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbMesItemStateChanged(evt);
            }
        });

        cbAnio.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbAnioItemStateChanged(evt);
            }
        });
        cbAnio.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                cbAnioMouseClicked(evt);
            }
        });

        jButton1.setText("Cerrar");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        btnGuardar.setText("Guardar");
        btnGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarActionPerformed(evt);
            }
        });

        lblEstado.setText("Estado");

        btnActualizar.setText("Actualizar");
        btnActualizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnActualizarActionPerformed(evt);
            }
        });

        btnLimpiar.setText("Limpiar");
        btnLimpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLimpiarActionPerformed(evt);
            }
        });

        btnEliminar.setText("Eliminar");
        btnEliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarActionPerformed(evt);
            }
        });

        btnBuscar.setText("Buscar");
        btnBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarActionPerformed(evt);
            }
        });

        cbPersona.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        comboPersona.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtId, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel2)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(txtRut, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnBuscar))
                            .addComponent(lblEstado)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel3)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel4)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtPaterno, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel5)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtMaterno, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addComponent(btnGuardar)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnActualizar)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnEliminar)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnLimpiar)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jButton1))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel6)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(cbAnio, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(cbMes, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(cbDia, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(cbPersona, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(comboPersona, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE)))
                        .addContainerGap())))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txtId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2)
                    .addComponent(txtRut, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnBuscar))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel5, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel3)
                        .addComponent(txtNombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel4)
                        .addComponent(txtPaterno, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txtMaterno, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cbDia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6)
                    .addComponent(cbMes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cbAnio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cbPersona, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(comboPersona, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblEstado)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton1)
                    .addComponent(btnGuardar)
                    .addComponent(btnActualizar)
                    .addComponent(btnLimpiar)
                    .addComponent(btnEliminar))
                .addContainerGap())
        );

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Lista Personas"));

        tabla.setAutoCreateRowSorter(true);
        tabla.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tabla.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablaMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tabla);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        dispose();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void btnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarActionPerformed
        // TODO add your handling code here:
        if(!this.setDatos()){
            
        } else if (pc.save()){
                this.estado(true, "Guardado con exito");
                
                this.conTexto();
                
                pc.findByRut();
                this.txtId.setText(String.valueOf(pc.getIdPersona()));
        } else {
            this.estado(false, "No se puede guardar el registro");
        }
    }//GEN-LAST:event_btnGuardarActionPerformed

    private void tablaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablaMouseClicked
        // TODO add your handling code here:
        int fila = tabla.rowAtPoint(evt.getPoint());
        
        if(fila > -1){
            this.txtId.setText(String.valueOf(tabla.getValueAt(fila,0)));
            this.txtRut.setText(String.valueOf(tabla.getValueAt(fila,1)));
            this.txtNombre.setText(String.valueOf(tabla.getValueAt(fila, 2)));
            this.txtPaterno.setText(String.valueOf(tabla.getValueAt(fila, 3)));
            this.txtMaterno.setText(String.valueOf(tabla.getValueAt(fila, 4)));
            
            String fechaCompleta = String.valueOf(tabla.getValueAt(fila, 5));
            
            String[] fecha = fechaCompleta.split("-");
            
            System.out.println("Fecha= 0:"+fecha[0]+" 1:"+fecha[1]+" 2:"+fecha[2]);

            this.cbAnio.setSelectedIndex(Integer.parseInt(fecha[0])-1899);
            this.cbMes.setSelectedIndex(Integer.parseInt(fecha[1]));
            this.cbDia.setSelectedIndex(Integer.parseInt(fecha[2])-1);
            
            this.lblEstado.setVisible(false);
            
            this.conTexto();
        }
    }//GEN-LAST:event_tablaMouseClicked

    private void btnActualizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnActualizarActionPerformed
        // TODO add your handling code here:
        if(!this.setDatos()){}
        else if(pc.update()){
            this.estado(true, "Registro actualizado");
            this.conTexto();
        } else {
            this.estado(false, "Error actualizando el registro");
        }     
    }//GEN-LAST:event_btnActualizarActionPerformed

    private void btnLimpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLimpiarActionPerformed
        // TODO add your handling code here:
        this.limpiar();
    }//GEN-LAST:event_btnLimpiarActionPerformed

    private void btnEliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarActionPerformed
        // TODO add your handling code here:
        int idPersona = Integer.parseInt(this.txtId.getText());
        
        pc.setIdPersona(idPersona);
        if(pc.delete()){
            this.estado(true, "Registro eliminado");
        } else {
            this.estado(false, "Error el aliminar registro");
        }
        
        this.limpiar();
    }//GEN-LAST:event_btnEliminarActionPerformed

    private void cbMesItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbMesItemStateChanged
        // TODO add your handling code here:
        this.setComboDia(this.cbMes.getSelectedIndex(), this.cbAnio.getSelectedIndex()+1899);
        this.cbDia.setEnabled(true);
    }//GEN-LAST:event_cbMesItemStateChanged

    private void cbAnioItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbAnioItemStateChanged
        // TODO add your handling code here:
        if(this.cbAnio.getSelectedIndex()!=0){
            this.cbMes.setEnabled(true);
        } else {
            this.cbMes.setEnabled(false);
            this.cbDia.setEnabled(false);
        }
    }//GEN-LAST:event_cbAnioItemStateChanged

    private void cbAnioMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_cbAnioMouseClicked
        // TODO add your handling code here:        
    }//GEN-LAST:event_cbAnioMouseClicked

    private void btnBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarActionPerformed
        // TODO add your handling code here:
        String rutPersona = this.txtRut.getText();
        
        System.out.println("Res="+pc.getNombrePersona());
        
        pc.setRutPersona(rutPersona);
        pc.findByRut();
        
        this.txtId.setText(String.valueOf(pc.getIdPersona()));
    }//GEN-LAST:event_btnBuscarActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnActualizar;
    private javax.swing.JButton btnBuscar;
    private javax.swing.JButton btnEliminar;
    private javax.swing.JButton btnGuardar;
    private javax.swing.JButton btnLimpiar;
    private javax.swing.JComboBox<String> cbAnio;
    private javax.swing.JComboBox<String> cbDia;
    private javax.swing.JComboBox<String> cbMes;
    private javax.swing.JComboBox<String> cbPersona;
    private javax.swing.JComboBox<String> comboPersona;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblEstado;
    private javax.swing.JTable tabla;
    private javax.swing.JTextField txtId;
    private javax.swing.JTextField txtMaterno;
    private javax.swing.JTextField txtNombre;
    private javax.swing.JTextField txtPaterno;
    private javax.swing.JTextField txtRut;
    // End of variables declaration//GEN-END:variables
}
