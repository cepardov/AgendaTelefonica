/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import javax.swing.JOptionPane;

/**
 *
 * @author cepardov
 */
public class DataBaseInstance {
    /**
     * Este es la segunda clase a crear que permitira a las clases del paquete dao
     * acceder a la instancia de la base de datos.
     * Primero: Se crean la variables que contendrán la información de la
     * conexión.
     */
    //Contendrá los datos de conexión como la session, este enviará y escribira declaraciones de SQL
    private static Connection conn; 
    
     //Se crean constantes no modificables usando static final https://www.geeksforgeeks.org/final-static-variable-java/
    private static final String baseDatos = "atelefonica";
    private static final String usuario = "root";
    private static final String contraseña = "123";
    private static final String port="3306";
    
    //Se elige el driver que se utilizará como MySql o Derby
    private static final String driver="com.mysql.jdbc.Driver";
    
    //Aqui le indicamos la dirección de nuestra base de datos usando el protocolo jdbc
    private static final String url="jdbc:mysql://localhost:"+port+"/"+baseDatos;
    
    Log log;
    /**
     * Conexion a mysql
     * @return Un metodo para conectar a mysql
     */
    public static Connection getInstanceConnection(){
        if (!(conn instanceof Connection)) {//Consultamos el estado de la conexion, si esta conectada retorna true.
            Log.INFO_FINAL("Conectando a la base de datos");
            try{
                Class.forName(driver);
                conn = DriverManager.getConnection(url, usuario, contraseña);
            } catch (ClassNotFoundException e){
                Log.ERROR_FINAL(""+e);
                JOptionPane.showMessageDialog(null," No se encuentra el controlador "+e.getLocalizedMessage(), "Error Grave", JOptionPane.ERROR_MESSAGE);
            } catch (SQLException e){
                Log.ERROR_FINAL(e.getErrorCode()+": la comunicación ha fallado.");
                JOptionPane.showMessageDialog(null,e.getSQLState()+" la comunicación ha fallado.", "Error Grave", JOptionPane.ERROR_MESSAGE);
            }
        }
        return conn;
    }
    
    public static void closeConnection(){
        try{
            if (conn instanceof Connection) {
                conn.close();
                conn = null;
                Log.INFO_FINAL("Se ha cerrado la conexión");
            }
        } catch (SQLException e){
            System.out.println("Error: "+e);
        }
    }
    
    public static boolean stateConnection(){
        return conn instanceof Connection;
    }

    public DataBaseInstance() {
        this.log = new Log();
    }
    
}
