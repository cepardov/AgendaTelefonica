/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Util;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author cepardov
 */
public class Log {
    
    private static String fechaHora(){
        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss.SSS");
        Date date = new Date();        
        
        String fh = formato.format(date);
        return fh;
    }
    
    public static final void INFO_FINAL(String info){
        System.out.println("["+fechaHora()+"] INFO: "+info);
    }
    
    public static final void ERROR_FINAL(String error) {
        System.err.println("["+fechaHora()+"] INFO: "+error);
    }
    
    public void INFO(String info){
        System.out.println("["+fechaHora()+"] INFO: "+info);
    }
    
    public void ERROR(String error) {
        System.err.println("["+fechaHora()+"] INFO: "+error);
    }
    
    
}
